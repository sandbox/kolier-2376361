<?php

require_once drupal_get_path('module', 'package') . '/package.field.inc';

// -----------------------------------------------------------------------------
// Hook

/**
 *  Implements hook_element_info().
 */
function package_element_info() {
  return array(
    'package' => array(
      '#input' => TRUE,
      '#tree' => TRUE,
      '#package' => package_element_default_settings(TRUE),
      //'#value_callback' => 'form_type_package_dev_value',
      '#process' => array('form_process_package'),
      '#pre_render' => array('package_pre_render'),
      '#default_value' => array(),
    ),
  );
}

/**
 * Helper function to get 'package' element default settings.
 */
function package_element_default_settings($info = FALSE) {
  return array(
    'id' => $info ? NULL : drupal_html_id('package'),
    'wrapper_id' => $info ? NULL : drupal_html_id('package-wrapper'),
    'table_id' => $info ? NULL : drupal_html_id('package-table-id'),
    'max_delta' => 100,
    'options_list' => NULL,
    'item_construct' => 'package_item_file',
  );
}

// -----------------------------------------------------------------------------
// Helper, Callback

/**
 * Value callback for 'package' element, prepare the '#value' property.
 */
function form_type_package_value($element, $input = FALSE, &$form_state) {
  if ($input === FALSE) {
    if (isset($element['#value'])) {
      return $element['#value'];
    }
    else {
      return $element['#default_value'];
    }
  }

  // NULL value when form is going to delete item or it's a real empty.
  if (!isset($input['pack']) && $input['attach']['value'] == 0) {
    return array();
  }

  $value = $input['pack'];

  if (isset($form_state['triggering_element'])) {
    // Removed item
    $removed = array();
    foreach ($input['pack'] as $id => $item) {
      if (
        (isset($item['remove']) && $item['remove'] === TRUE)
        || (!isset($item['name']) && !isset($item['description']))
      ) {
        $removed[] = $id;
      }
    }
    foreach ($removed as $removed_id) {
      unset($value[$removed_id]);
    }

    // New attached item, only action if click 'Add' button.
    $parents_check = array_slice($form_state['triggering_element']['#array_parents'], -2);
    if ($parents_check[0] == 'attach' && $parents_check[1] = 'add' && $input['attach']['value'] > 0) {
      $attach_id = (int) $input['attach']['value'];
      if (!array_key_exists($attach_id, $value)) {
        $value[$attach_id] = package_file_item($input['attach']['value']);
      }
    }
  }

  return $value;
}

function package_value_filter($value, $key) {
  if (in_array($key, array('attach', 'pack'))) {
    return FALSE;
  }
  return TRUE;
}

function package_file_item($fid) {
  $file = file_load($fid);
  return array(
    'fid' => $fid,
    'name' => $file->filename,
    'description' => $file->filename,
  );
}

/**
 * 'process' callback for 'package' element.
 */
function form_process_package($element, &$form_state, &$complete_form) {
  // Wrapper
  $element += array(
    '#prefix' => '<div id="' . $element['#package']['wrapper_id'] . '">',
    '#suffix' => '</div>',
  );

  $package = $element['#package'];

  // Pack
  $element['pack'] = array();
  $element_value = $element['#value'];
  $weight = 0;
  foreach ($element_value as $id => $item) {
    $element['pack'][$id] = $package['item_construct']($id, $item, $weight, $package['max_delta'], $weight, $element, $form_state, $complete_form);
    $weight++;
  }

  // Attach
  $element['attach'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attach new item'),
    /*
    '#attributes' => array(
      'class' => array('form-inline'),
    ),
    */
  );
  $options_list = array();
  $options_list_callback = $element['#package']['options_list'];
  if (isset($options_list_callback) && function_exists($options_list_callback)) {
    $options_list = $options_list_callback($element, $form_state, $complete_form);
  }
  if (!empty($options_list)) {
    $element['attach']['value'] = array(
      '#type'          => 'select',
      '#default_value' => '',
      '#options'       => $options_list,
      '#empty_value'   => 0,
      '#title'         => t('Item id'),
      '#title_display' => 'invisible',
      // @todo Temporary solution, potential security issue.
      // @link https://www.drupal.org/node/1231990
      '#validated'     => TRUE,
    );
    $element['attach']['add'] = array(
      '#type'  => 'submit',
      '#name'  => str_replace('-', '_', $package['id']) . '_attach_add',
      '#value' => t('Add'),
      '#submit' => array('package_attach_add_submit'),
      '#ajax'  => array(
        'callback' => 'package_attach_add_ajax',
        'wrapper'  => $package['wrapper_id'],
      ),
    );
  }

  return $element;
}

/**
 * A default callback to construct item, it can be override.
 *
 * @todo Solve field issue.
 * @see package_field_item_file().
 */
function package_item_file($id, $item, $delta, $max_delta, $weight, $element, $form_state, $complete_form) {
  $parents = array_slice($element['#parents'], 1);
  $name = $element['#parents'][0] . '[' . implode('][', $parents) . '][pack][' . $id . ']';
  $item_file = array(
    'id'         => array(
      '#markup' => check_plain($id),
    ),
    'name'        => array(
      '#type'          => 'textfield',
      '#default_value' => check_plain($item['name']),
      '#size'          => 40,
      '#maxlength'     => 255,
    ),
    'description' => array(
      '#type'          => 'textfield',
      '#default_value' => check_plain($item['description']),
      '#size'          => 40,
      '#maxlength'     => 255,
    ),
    'remove'      => array(
      '#type'          => 'submit',
      '#name'          => $name . '[remove]',
      '#value'         => t('Remove'),
      '#submit'        => array('package_item_file_remove_submit'),
      '#ajax'          => array(
        'callback' => 'package_item_remove_ajax',
        'wrapper'  => $element['#package']['wrapper_id'],
      ),
    ),
    'weight'      => array(
      '#type'          => 'weight',
      '#title'         => t('Weight'),
      '#default_value' => $weight,
      '#delta'         => $max_delta,
      '#title_display' => 'invisible',
    ),
  );

  return $item_file;
}

/**
 * Submit callback for remove button.
 */
function package_item_file_remove_submit($elements, &$form_state) {
  drupal_array_set_nested_value($form_state['values'], $form_state['triggering_element']['#array_parents'], TRUE);
  drupal_array_set_nested_value($form_state['input'], $form_state['triggering_element']['#array_parents'], TRUE);

  $form_state['rebuild'] = TRUE;
}

/**
 * Helper function to get the 'package' element by 'triggering_element'.
 */
function package_form_get_element($form, $form_state, $depth) {
  $array_parents = $form_state['triggering_element']['#array_parents'];
  $array_parents = array_slice($array_parents, 0, $depth);
  $element = drupal_array_get_nested_value($form, $array_parents);

  return $element;
}

/**
 * Ajax callback.
 */
function package_attach_add_ajax($form, &$form_state) {
  $element = package_form_get_element($form, $form_state, -2);

  return $element;
}

function package_attach_add_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback.
 */
function package_item_remove_ajax($form, &$form_state) {
  $element = package_form_get_element($form, $form_state, -3);

  return $element;
}

/**
 * 'pre_render' callback for 'package' element.
 */
function package_pre_render($elements) {
  $rows = array();
  foreach (element_children($elements['pack']) as $id) {
    $elements['pack'][$id]['weight']['#attributes']['class'] = array('package-pack-item-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($elements['pack'][$id]['id']),
        drupal_render($elements['pack'][$id]['name']),
        drupal_render($elements['pack'][$id]['description']),
        drupal_render($elements['pack'][$id]['remove']),
        drupal_render($elements['pack'][$id]['weight']),
      ),
      'class' => array('draggable'),
    );
  }
  $header = array(t('ID'), t('Name'), t('Description'), t('Remove'), t('Weight'));
  $table_id = $elements['#package']['table_id'];
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'package-pack-item-weight');

  $elements['pack']['#children'] = $output;

  return $elements;
}
